# RChat

[![CI Status](https://img.shields.io/travis/iOSDeveloper/RChat.svg?style=flat)](https://travis-ci.org/iOSDeveloper/RChat)
[![Version](https://img.shields.io/cocoapods/v/RChat.svg?style=flat)](https://cocoapods.org/pods/RChat)
[![License](https://img.shields.io/cocoapods/l/RChat.svg?style=flat)](https://cocoapods.org/pods/RChat)
[![Platform](https://img.shields.io/cocoapods/p/RChat.svg?style=flat)](https://cocoapods.org/pods/RChat)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RChat is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RChat'
```

## Author

iOSDeveloper, iosdeveloper77@gmail.com

## License

RChat is available under the MIT license. See the LICENSE file for more info.
